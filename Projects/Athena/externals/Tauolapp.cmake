#
# File specifying the location of Tauola++ to use.
#

set( TAUOLAPP_LCGVERSION 1.1.6 )
set( TAUOLAPP_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/tauola++/${TAUOLAPP_LCGVERSION}/${LCG_PLATFORM} )
